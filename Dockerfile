FROM tiangolo/uvicorn-gunicorn-fastapi:python3.7 

#---- SSH FORWARDING (not the best way) ----#
# RUN apt-get update -y
# RUN apt-get install -y ssh

# ARG SSH_KEY

# RUN mkdir -p /root/.ssh && chmod 0700 /root/.ssh && \
#   ssh-keyscan bitbucket.org > /root/.ssh/known_hosts

# RUN echo "$SSH_KEY" > /root/.ssh/id_rsa &&\
#   chmod 0700 /root/.ssh/id_rsa
#---- ----#

COPY ./app /app
COPY ./requirements.txt .
RUN pip install -r ./requirements.txt
# RUN pip install git+ssh://git@bitbucket.org/ekimetrics/mmm-response-curve.git@v0.1.2 (Example of custom package installation)
