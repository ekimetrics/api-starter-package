# API STARTER PACKAGE #

Easy-to-use and production-ready platform to create APIs.

### Quick summary ###

### Version ###
**v0.1.0**

### Summary of set up ###

### Configuration ###

Prerequisites:

- Python **3.6** or higher
- IDE: **Visual Studio Code (VSCODE)** and some interesting extensions (see below)
- You need the **.env file** to make it work on your local machine. You can rename the '.sample.env' file ;)

Recommended tools & plugins:

- **Sonarlint**: - (VSCode plugin) - Detect and fix quality issues. [SonarLint](https://www.sonarlint.org/)
- **PyLint** - (VSCode plugi )[Pylint](https://code.visualstudio.com/docs/python/linting)
- **Postman** - Simplify each step of building an API and streamline collaboration so you can create better APIs—faster. [Postman](https://www.postman.com/)


Clone the repository locally: 
```bash
git clone git@bitbucket.org:ekimetrics/api-starter-package.git
```
BEST PRACTICE

Use a virtual environment :

Make sure you have **virtualenv** installed:

```bash
pip install virtualenv
```
Create a venv folder
```bash
virtualenv venv
```
Activate your venv
```bash
source venv/bin/activate
```
Your development environment is now fully isolated ! You can test whatever you want :)

Install the required packages: 
```bash
pip install -r requirements.txt
```

Go to 'app' folder and startup the server: 
```bash
python app/main.py
```

To deactive your virtual env: 
```bash
deactivate
```

### Dependencies ###
All the depencies are included in requirements.txt.

- **fastapi (v0.61.2)** : FastAPI is a modern, fast (high-performance), web framework for building APIs with Python 3.6+. [FastAPI](https://fastapi.tiangolo.com/)
- **uvicorn (v0.12.2)** : Uvicorn is a lightning-fast ASGI server implementation, using uvloop and httptools. [Uvicorn](https://www.uvicorn.org/)
- **pydantic (v1.7.2)** : Data validation and settings management using python type annotations. [Pydantic](https://pydantic-docs.helpmanual.io/)
- **python-dotenv (v0.13.0)** : Reads the key-value pair from .env file and adds them to environment variable. It is great for managing app settings during development and in production using 12-factor principles. [DotEnv](https://pypi.org/project/python-dotenv/)
- **motor (v2.0.3)** : Asynchronous Python driver for MongoDB. [Motor](https://motor.readthedocs.io/en/stable/)
- **pytest (v6.1.2)** : The pytest framework makes it easy to write small tests, yet scales to support complex functional testing for applications and libraries.. [Pytest](https://pypi.org/project/pytest/)

### Database configuration ###
Database configuration should be entered in a .env file (see example : .env.sample).

**As the file contains secrets (such as db credentials), it must not be pushed to the repo: make sure to keep it into your .gitgnore.**

### How to run tests ###

### Deployment instructions ###
Deployments are conducted by bitbuckets pipelines and automaticaly triggered by pushing on specific branches:

- branch **'development'** -> deploy to **development environment**
- branch **'preprod'** -> **preprod environment**
- brnach **'production'** -> **production environment**

To be ready to deploy make sure you have ordered your cloud ressources !

Create a ticket to IT on [freshervice](https://ekimetrics.freshservice.com/support/catalog/items/72) & ask politely for the following ressoures :

- An App service
- A Container Registry

Here is the documentation explaining how to deploy the API on Azure :
[HowToDeploy](https://ekimetrics.atlassian.net/wiki/spaces/TMANUF/pages/2350285351/How+to+deploy+the+API)

### Package structure ###
------------

    ├── app
    │   ├── core                  <- Data from third party sources.
    │   │  └── config             <- Config class.
    │   │  └── middlewares        <- Security Middlewares.    
    │   │  └── db                 <- DB Clients.
    │   ├── models                <- Database Models
    │   │  └── item.py                           
    │   ├── routers               <- Routers: routes are defined there. The approach: 1 router per model.
    │   │   └── items.py          <- All routes related to items. With basic operations (CRUD: Create/Read/Update/Delete)
    │   │   └── my_algo.py        <- Custom endpoints
    │   ├── services              <- Custom libraries. Logic must be here.
    │   │   └── operations.py
    │   ├── tests                 <- Both unit & End-to-End tests.
    │   │   └── test_main.py
    │   └── main.py               <- Fastapi app is defined here.
    ├── docs                      <- Any docs that may help
    ├── README.md                 <- The top-level README for developers using this project.
    ├── bitbucket-pipelines.yml   <- Pipeline file to trigger automatic deployments
    ├── requirements.txt          <- Pyton dependencies (to be compeleted if needed)
    ├── Dockerfile                <- Dockerfile to create environment on deployed web apps. (DO NOT MODIFY IT)
    └── sample.env                <- Example of .env file with configuration. Please use your own .env.
--------

### Contribution guidelines ###
### Writing tests ###

Pytest is the recommended libary.
Some very simple tests are availaable in the 'tests' folder.
To launch the tests:

```bash
cd app
python -m pytest -v
```

##### Code review #####


##### Bitbucket good practices #####
- **1 feature/bugfix = 1 branch**
- **Never commit directly** on either development/preprod/master branch.
- Always create a Pull Request & add as many reviewers as possible

##### Install Eki's python packages on Bitbucket #####
```bash
pip install git+ssh://git@bitbucket.org/ekimetrics/**{package_name}**.git@v0.1
```
Pre-requisite : Add a SSH key to the targeted repo

Exemple:
```bash
pip install git+ssh://git@bitbucket.org/ekimetrics/mmm-response-curve.git@v0.1
```
To be able to run pip install git+ssh:// into a bitbucket pipeline, you need to:

- In your API package, open Settings & go to Pipelines > SSH Keys
- Generate a new SSH key
- Copy the pulbic key
- Go to the target package in bitbucket, Settings > Access Keys, and click on"Add a new key". Enter a relevant name and paste the public key.

##### Other guidelines #####

### CONTACTS ###

Owner: inno@ekimetrics.com

Main contact: jerome.guichard@ekimetrics.com

### Userful links ###
[Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)