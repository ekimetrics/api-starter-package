from pydantic import BaseModel, Field
from typing import Optional
from core.db.pydanticobjectid import PydanticObjectId


class Item(BaseModel):
    id: Optional[PydanticObjectId] = Field(alias="_id")
    ref: int
    name: Optional[str]
    description: Optional[str]
    price: Optional[float]

    class Config:
        schema_extra = {
            "example": {
                "ref": "1",
                "name": "Item A",
                "description": "Very nice item",
                "price": 4000,
            }
        }
