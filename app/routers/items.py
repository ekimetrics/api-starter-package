from fastapi import APIRouter, HTTPException, Body
from typing import List
from models.item import Item
from core.db.mongodb import get_database
from fastapi.encoders import jsonable_encoder
from bson.objectid import ObjectId
from starlette.status import (
    HTTP_200_OK,
    HTTP_201_CREATED,
    HTTP_204_NO_CONTENT,
    HTTP_404_NOT_FOUND,
)

router = APIRouter()

ITEM_NOT_FOUND_MSG = "Item not found"


@router.get(
    "/items", tags=["Items"], response_model=List[Item], status_code=HTTP_200_OK
)
async def get_item_list():
    """
    Get list of items

    """
    items: List[Item] = []
    db = get_database()
    cursor = db.items.find({})
    async for doc in cursor:
        items.append(doc)

    return items


@router.get("/items/{id}", tags=["Items"], response_model=Item, status_code=HTTP_200_OK)
async def get_item(id: str):
    """
    Get single item

    - **id**: 'id in db'
    """
    db = get_database()
    item = await db.items.find_one({"_id": ObjectId(id)})
    if item is None:
        raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail=ITEM_NOT_FOUND_MSG)
    return item


@router.post("/items", tags=["Items"], status_code=HTTP_201_CREATED)
async def create_item(item: Item = Body(...)):
    """
    Create a new item

    """

    item = jsonable_encoder(item)
    db = get_database()
    await db.items.insert_one(item)
    return {"msg": "created"}


@router.put("/items/{id}", tags=["Items"], status_code=HTTP_200_OK)
async def update_item(id: str, item: Item = Body(...)):
    """
    Update an existing item

    - **id**: id of the item

    """
    db = get_database()
    item = jsonable_encoder(item)
    result = await db.items.update_one({"_id": ObjectId(id)}, {"$set": item})

    if result.matched_count == 0:
        raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail=ITEM_NOT_FOUND_MSG)
    return {"msg": "OK"}


@router.delete("/items/{id}", tags=["Items"], status_code=HTTP_204_NO_CONTENT)
async def delete_item(id: str):
    """
    Delete item

    - **id**: id of the item
    """
    db = get_database()
    result = await db.items.delete_one({"_id": ObjectId(id)})
    if result.deleted_count == 0:
        raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail=ITEM_NOT_FOUND_MSG)
