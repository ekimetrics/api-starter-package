from fastapi import APIRouter
from starlette.status import HTTP_200_OK
from services.operations import add, substract

router = APIRouter()


@router.get("/addition", tags=["Algo Example"], status_code=HTTP_200_OK)
async def get_add(a: int = 0, b: int = 0):
    """
    Add 2 integers a & b

    - **a**: integer
    - **b**: integer

    Returns the addition integer
    """
    return add(a, b)


@router.get("/substraction", tags=["Algo Example"], status_code=HTTP_200_OK)
async def get_substract(a: int = 0, b: int = 0):
    """
    Substract 2 integers a & b

    - **a**: integer
    - **b**: integer

    Returns an integer
    """
    return substract(a, b)
