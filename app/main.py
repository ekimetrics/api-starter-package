from fastapi import FastAPI
import uvicorn
import logging

# Config #
from core.config import settings

# Middleware ##
from core.middlewares.tokenmiddelware import TokenMiddelware

# DB ##
from core.db.mongodb import setup_mongo_client, close_mongo_connection

# Routers #
from routers import items, my_algo


# Metadata ##
openapi_tags = [
    {
        "name": "Items",
        "description": "Endpoints related to 'Items'."
        + "It's an example of a CRUD (Create/Read/Update/Delete).",
        "externalDocs": {
            "description": "FastAPI external docs",
            "url": "https://fastapi.tiangolo.com/",
        },
    },
]

# Logger definition
logging.basicConfig(
    format="[%(process)d] [%(levelname)s] %(message)s", level=logging.DEBUG
)
logging.info("Starting up the app..")

# Create APP
app = FastAPI(
    title="Starter API Package",
    description="Easy-to-use and production-ready platform to create APIs",
    version="0.1.1",
    openapi_tags=openapi_tags,
    docs_url=settings.get_settings().docs_url,
)

logging.info("App started.")


@app.on_event("startup")
async def create_db_client():
    logging.info("Starting MMO API & connect to MongoDB..")
    await setup_mongo_client()


@app.on_event("shutdown")
async def shutdown_db_client():
    logging.info("Shutting down API & close connection to MongoDB..")
    await close_mongo_connection()


# ADD ROUTERS ##
app.include_router(items.router, prefix=settings.get_settings().api_prefix)
app.include_router(my_algo.router, prefix=settings.get_settings().api_prefix)


# ADD CUSTOM TOKEN MIDDELWARE ##
app.add_middleware(
    TokenMiddelware,
    is_protected=settings.get_settings().enable_token_protection,
    api_key=settings.get_settings().api_key,
)


# TO BE REMOVED
@app.get("/hello")
async def hello_world():
    return {
        "msg": "Hello World",
    }


if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=8000, reload=True, lifespan="on")
