from fastapi.testclient import TestClient
from app.main import app
from services.operations import add, substract

client = TestClient(app)


# Simple unit tests
def test_operation_add():
    assert add(3, 4) == 7


def test_operation_substract():
    assert substract(4, 1) == 3


# END-to-END tests
def test_add_a_b():
    response = client.get(
        "/api/v1/addition?a=4&b=2", headers={"HTTP_API_KEY": "do_not_set_it_here"}
    )
    assert response.status_code == 200
    assert response.json() == 6


def test_sub_a_b():
    response = client.get(
        "/api/v1/substraction?a=4&b=2", headers={"HTTP_API_KEY": "do_not_set_it_here"}
    )
    assert response.status_code == 200
    assert response.json() == 2


def test_add_default_a_b():
    response = client.get(
        "/api/v1/substraction?c=4&d=2", headers={"HTTP_API_KEY": "do_not_set_it_here"}
    )
    assert response.status_code == 200
    assert response.json() == 0


def test_hello_world_no_api_key():
    response = client.get("hello")
    assert response.status_code == 401


def test_hello_world_wrong_api_key():
    response = client.get("hello", headers={"HTTP_API_KEY": "hailhydra"})
    print(response)
    assert response.status_code == 401


def test_hello_world_correct_api_key():
    response = client.get("hello", headers={"HTTP_API_KEY": "do_not_set_it_here"})
    print(response)
    assert response.status_code == 200
    assert response.json() == {"msg": "Hello World"}
