from starlette.requests import Request
from starlette.responses import Response
from starlette.types import ASGIApp, Receive, Scope, Send
from starlette.status import HTTP_401_UNAUTHORIZED


class TokenMiddelware(object):
    def __init__(self, app: ASGIApp, is_protected: bool, api_key: str):
        self.app = app
        self.is_protected = is_protected
        self.api_key = api_key

    async def __call__(self, scope: Scope, receive: Receive, send: Send):

        if scope["type"] == "lifespan":
            await self.app(scope, receive, send)
            return

        # Convert into nicer req object
        assert scope["type"] == "http"
        request = Request(scope, receive)

        # Check API Token
        allowed_request: bool = check_api_token(
            request, self.is_protected, self.api_key
        )

        # Return 401 if not allowed
        if allowed_request is False:
            response = Response("Unauthorized", status_code=HTTP_401_UNAUTHORIZED)
            await response(scope, receive, send)
            return

        await self.app(scope, receive, send)


def check_api_token(request: Request, is_protected: bool, api_key: str):
    if is_protected is True:
        return (
            True
            if "HTTP_API_KEY" in request.headers
            and request.headers["HTTP_API_KEY"] == api_key
            else False
        )
    else:
        return True
