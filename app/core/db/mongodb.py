from motor.motor_asyncio import AsyncIOMotorClient
from core.config import settings


class MongoClient:
    client: AsyncIOMotorClient = None


db = MongoClient()


def get_database() -> AsyncIOMotorClient:
    return db.client[settings.get_settings().db_name]


async def setup_mongo_client() -> None:
    db.client = AsyncIOMotorClient(
        str(settings.get_settings().db_uri), maxPoolSize=10, minPoolSize=10
    )


async def close_mongo_connection() -> None:
    db.client.close()
