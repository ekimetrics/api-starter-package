from functools import lru_cache
from pydantic import BaseSettings


class Settings(BaseSettings):

    # Common settings
    api_prefix: str = "/api/v1"
    docs_url: str = "/docs"
    debug: bool = False
    testing: bool = False

    # DB related settings
    db_name: str = "do_not_set_it_here"
    db_uri: str = "user_a_.env_file"

    # Security settings
    enable_token_protection: bool = True
    api_key: str = "do_not_set_it_here"

    class Config:
        env_file = ".env"  # .ENV file is used to store db accesses and credentials


@lru_cache()
def get_settings() -> Settings:
    print("I create the settings only once")
    return Settings()
